package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import java.awt.Color;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.SpringLayout;
import javax.swing.border.MatteBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import javax.swing.JToggleButton;
import java.awt.Component;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Rectangle;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import javax.swing.border.LineBorder;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.event.ChangeListener;

import juego.Juego;

import javax.swing.event.ChangeEvent;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextArea;
import java.awt.FlowLayout;
import java.awt.SystemColor;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

public class JuegoInterfaz {

	private JFrame frame;

	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JuegoInterfaz window = new JuegoInterfaz();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JuegoInterfaz() {
		Juego juego = new Juego();
		juego.generarMatriz();
		initialize(juego.getMatriz(), juego);	
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize(boolean[][] matrizJuego, Juego juego) {
		frame = new JFrame();
		frame.getContentPane().setVisible(true);
		frame.setBounds(100, 100, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setForeground(new Color(0, 128, 64));
		frame.getContentPane().setBackground(new Color(224, 255, 255));
		frame.getContentPane().setLayout(null);

		
		JButton fil0col0 = new JButton("");
		fil0col0.setBackground(new Color(255, 250, 205));
		fil0col0.setBounds(50, 50, 90, 50);
		frame.getContentPane().add(fil0col0);
		
		JButton fil1col0 = new JButton("");
		fil1col0.setBounds(50, 120, 90, 50);
		frame.getContentPane().add(fil1col0);
		
		JButton fil2col0 = new JButton("");
		fil2col0.setBounds(50, 190, 90, 50);
		frame.getContentPane().add(fil2col0);
		
		JButton fil3col0 = new JButton("");
		fil3col0.setBounds(50, 260, 90, 50);
		frame.getContentPane().add(fil3col0);
		
		JButton fil0col1 = new JButton("");
		fil0col1.setBounds(150, 50, 90, 50);
		frame.getContentPane().add(fil0col1);
		
		JButton fil1col1 = new JButton("");
		fil1col1.setBounds(150, 121, 90, 50);
		frame.getContentPane().add(fil1col1);
		
		JButton fil2col1 = new JButton("");
		fil2col1.setBounds(150, 190, 90, 50);
		frame.getContentPane().add(fil2col1);
		
		JButton fil3col1 = new JButton("");
		fil3col1.setBounds(150, 260, 90, 50);
		frame.getContentPane().add(fil3col1);
		
		JButton fil0col2 = new JButton("");
		fil0col2.setBounds(250, 50, 90, 50);
		frame.getContentPane().add(fil0col2);
		
		JButton fil1col2 = new JButton("");
		fil1col2.setBounds(250, 120, 90, 50);
		frame.getContentPane().add(fil1col2);
		
		JButton fil2col2 = new JButton("");
		fil2col2.setBounds(250, 190, 90, 50);
		frame.getContentPane().add(fil2col2);
		
		JButton fil3col2 = new JButton("");
		fil3col2.setBounds(250, 260, 90, 50);
		frame.getContentPane().add(fil3col2);
		
		JButton fil0col3 = new JButton("");
		fil0col3.setBounds(350, 50, 90, 50);
		frame.getContentPane().add(fil0col3);
		
		JButton fil1col3 = new JButton("");
		fil1col3.setBounds(350, 120, 90, 50);
		frame.getContentPane().add(fil1col3);
		
		JButton fil2col3 = new JButton("");
		fil2col3.setBounds(350, 190, 90, 50);
		frame.getContentPane().add(fil2col3);
		
		JButton fil3col3 = new JButton("");
		fil3col3.setBounds(350, 260, 90, 50);
		frame.getContentPane().add(fil3col3);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(240, 255, 255));
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.setBounds(50, 330, 390, 200);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JButton volverJugar = new JButton("Volver a jugar");
		volverJugar.setVisible(false);
		volverJugar.setBounds(100, 140, 200, 50);
		panel.add(volverJugar);
		volverJugar.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		volverJugar.setHorizontalTextPosition(SwingConstants.CENTER);
		volverJugar.setFont(new Font("Tahoma", Font.BOLD, 10));
		
		JLabel contadorClicks = new JLabel("Contador Clicks");
		contadorClicks.setBounds(10, 10, 290, 50);
		panel.add(contadorClicks);
		contadorClicks.setBorder(null);
		contadorClicks.setFont(new Font("Arial", Font.BOLD, 24));
		contadorClicks.setHorizontalAlignment(SwingConstants.LEFT);
		
		JLabel recordClicks = new JLabel("Record");
		recordClicks.setBounds(10, 100, 290, 50);
		panel.add(recordClicks);
		recordClicks.setBorder(null);
		recordClicks.setFont(new Font("Arial", Font.BOLD, 24));
		recordClicks.setHorizontalAlignment(SwingConstants.LEFT);
		
		JTextPane paneVictoria = new JTextPane();
		paneVictoria.setBackground(new Color(224, 255, 255));
		paneVictoria.setEditable(false);
		paneVictoria.setVisible(false);
		paneVictoria.setBounds(50, 50, 390, 259);
		frame.getContentPane().add(paneVictoria);
		
		JLabel mensajeVictoria = new JLabel("GANASTE EL JUEGO!");
		mensajeVictoria.setBounds(50, 50, 390, 50);
		mensajeVictoria.setVisible(false);
		mensajeVictoria.setForeground(new Color(50, 205, 50));
		mensajeVictoria.setFont(new Font("Arial", Font.BOLD, 24));
		paneVictoria.add(mensajeVictoria);
		
		
		
		JButton matriz[][] = new JButton[4][4];
		
		matriz[0][0] = fil0col0;
		matriz[0][1] = fil0col1;
		matriz[0][2] = fil0col2;
		matriz[0][3] = fil0col3;
		matriz[1][0] = fil1col0;
		matriz[1][1] = fil1col1;
		matriz[1][2] = fil1col2;
		matriz[1][3] = fil1col3;
		matriz[2][0] = fil2col0;
		matriz[2][1] = fil2col1;
		matriz[2][2] = fil2col2;
		matriz[2][3] = fil2col3;
		matriz[3][0] = fil3col0;
		matriz[3][1] = fil3col1;
		matriz[3][2] = fil3col2;
		matriz[3][3] = fil3col3;
		
	
		
		
		//Boton Volver a jugar
		volverJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				juego.generarMatriz();
				
				for(int i=0;i<matriz.length;i++) {
					for(int j=0; j<matriz.length; j++) {
						
						matriz[i][j].setVisible(true);
					}
				}
				
				
				
				for(int i=0;i<matriz.length;i++) {
					for(int j=0; j<matriz.length; j++) {	
						if(matrizJuego[i][j]) {
							matriz[i][j].setBackground(new Color(127, 255, 0));
						}else {
							matriz[i][j].setBackground(new Color(255, 250, 205));
						}
					}
				}
				
				contadorClicks.setText("Contador Clicks " + juego.getContadorClicks());
				recordClicks.setText("Record " + juego.getRecord());
				
				volverJugar.setVisible(false);
				contadorClicks.setVisible(true);
				mensajeVictoria.setVisible(false);
			}
		});
			
		
		for(int i=0;i<matriz.length;i++) {
			for(int j=0; j<matriz.length; j++) {
				if(matrizJuego[i][j]) {
					matriz[i][j].setBackground(new Color(127, 255, 0));
				}else {
					matriz[i][j].setBackground(new Color(255, 250, 205));
				}
				
			}
		}
		



		//Cambiar estados fila/columna al clickear
		for(int i = 0 ; i < matriz.length ; i++) {
			for(int j = 0 ; j < matriz.length ; j++){
				
				int fila=i;
				int columna=j;
				
				matriz[i][j].addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						
						
						
						
						contadorClicks.setText("Contador Clicks " + juego.getContadorClicks());
						recordClicks.setText("Record " + juego.getRecord());
		
						
						if(juego.getContadorClicks()==20) {
							juego.hacerTodoFalso();
						}
						
						juego.modificarEstado(fila, columna);
						
						
						for(int i=0;i<matriz.length;i++) {
							for(int j=0; j<matriz.length; j++) {
								
								if(matrizJuego[i][j]) {
									matriz[i][j].setBackground(new Color(127, 255, 0));
								}else {
									matriz[i][j].setBackground(new Color(255, 250, 205));
								}
							}
						}
						
						
						
			            //GANO				
						if(juego.comprobarMatrizFalsa()) {
							paneVictoria.setVisible(true);
							mensajeVictoria.setVisible(true);
							volverJugar.setText("Volver a jugar");
				            volverJugar.setVisible(true);
				           
				            
				            for(int i=0;i<matriz.length;i++) {
								for(int j=0; j<matriz.length; j++) {
									
									matriz[i][j].setVisible(false);
								}
							}
						}
					}
				});
			}
		}
		
		
		
		
		
		
	}
}


