package juego;

import java.util.LinkedList;
import java.util.Random;

public class Juego {
	private boolean [][] matrizJuego;
	private int record;
	private int contadorClicks;
	//
	private boolean [][] matrizGeneradaInicial;
	private LinkedList <String> pistas;
	

	public Juego() {
		this.matrizJuego = new boolean[4][4];
		this.record=0;
		this.contadorClicks=0;
		matrizGeneradaInicial = new boolean [4][4];
		pistas = new LinkedList<String>();
		
	}

	public void mostrarMatriz(boolean[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
	}
	

	public void generarMatriz() {

		
		do {
			matrizJuego = llenarMatrizRandom(matrizJuego);
		}while(comprobarMatrizFalsa());
		
		mostrarPistas();
	}
	
	public void mostrarPistas() {
		
		for(String s:pistas) {
			System.out.println(s);
		}
		System.out.println(pistas.size());
	}
	
	public void hacerTodoFalso() {
		for (int i = 0; i < matrizJuego.length; i++) {
			for (int j = 0; j < matrizJuego.length; j++) {
				matrizJuego[i][j]=false;
			}
			
		}
	}
	
	public boolean comprobarMatrizFalsa() { //Retorna true si la matriz es falsa
		boolean ret = true;

		for (int i = 0; i < matrizJuego.length; i++) {
			for (int j = 0; j < matrizJuego.length; j++) {
				ret = ret && !matrizJuego[i][j]; // Hay una luz encendida
			}
		}
		
		if(ret) {
			comprobarRecord();
		}
		
		return ret;
	}

	private boolean[][] llenarMatrizRandom(boolean[][] matriz) {

		llenarMatrizDeFalsos();
		agregarJugadasMatriz();
		copiarMatrizInicial();
		
		return matriz;
	}
	
	private void copiarMatrizInicial() {
		
		for (int i = 0; i < matrizJuego.length; i++) {
			for (int j = 0; j < matrizJuego.length; j++) {
				matrizGeneradaInicial[i][j] = matrizJuego[i][j];
			}
		}
		
	}
	
	private void llenarMatrizDeFalsos(){
		for (int i = 0; i < matrizJuego.length; i++) {
			for (int j = 0; j < matrizJuego.length; j++) {
				matrizJuego[i][j] = false;
			}
		}
	}
	
	private void agregarJugadasMatriz() {
		Integer fila;
		Integer col;
		Random rdm = new Random();
		//Generar 10 clicks randoms
		
		for(int i = 0 ; i < 10; i++) {
			fila = rdm.nextInt(4); //Genera numero random entre 0 y 3
			col = rdm.nextInt(4);
			
			modificarEstado(fila,col); //revisar este metodo porque no se hizo click y aumenta contador
			
			fila+=1;
			col+=1;
			
			pistas.add("fila " + fila + " , " + "columna " + col);
			
		}
	}

	public void modificarEstado(int fila, int col) {
		
			
		
			modificarEstadoFila(col, matrizJuego[fila]);
			modificarEstadoColumna(col, matrizJuego);
			modificarEstadoPosicion(fila, col, matrizJuego);
			
			incrementarContadorClicks();
			
			
	}
	
	private void incrementarContadorClicks() {
		contadorClicks+=1;
	}
	
	private void comprobarRecord() {
		if(record == 0 || contadorClicks<record) {
			this.record = contadorClicks;
		}
		
		contadorClicks=0; //Vuelve 0 debido a que ya gano 
		pistas.removeAll(pistas);
	}
	
	

	public int getRecord() {
		return record;
	}

	public int getContadorClicks() {
		return contadorClicks;
	}

	private void modificarEstadoPosicion(int fila, int col, boolean matriz[][]) {
		if (matriz[fila][col] == true) {
			matriz[fila][col] = false;
		} else {
			matriz[fila][col] = true;
		}
	}

	private void modificarEstadoFila(int col, boolean vector[]) {
		for (int i = 0; i < vector.length; i++) {

			if (vector[i] == true) {
				vector[i] = false;
			} else {
				vector[i] = true;
			}
		}
	}

	private void modificarEstadoColumna(int col, boolean matriz[][]) {
		for (int i = 0; i < matriz[col].length; i++) {
			if (matriz[i][col] == true) {
				matriz[i][col] = false;
			} else {
				matriz[i][col] = true;
			}
		}
	}
	
	public boolean[][] getMatriz(){
		return this.matrizJuego;
	}

}
